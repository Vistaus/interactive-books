![InteractiveBooks](https://gitlab.com/cibersheep/interactive-books/raw/master/assets/logo.svg)

# Interactive Books

App to read interactive books based on choices.

## Icons used
 - Floral elements: CC-By LSE Designs, CC-By Jacqueline Fernandes
 - Main icon: CC-by Gregor Cresnar

## Books, books, book?

At the [Archive](https://archive.org/download/ZorkTheForcesOfKrill/Zork-The_Forces_of_Krill-%281.0%29.ibs)

Create your own book using the information on the [wiki](https://gitlab.com/cibersheep/interactive-books/-/wikis/home) of the project

## Translations

Please, feel free to translate this app to your language using [Weblate](https://translate-ut.org/projects/interactivebooks/interactive-books/)

## Get the app from the OpenStore

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/interactivebooks.cibersheep)

