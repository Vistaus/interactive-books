import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0

import "components"
import "js/db.js" as DB

Page {
    id: bookView
    property url bookPath
    property var bookIndex

    header: BookHeader {
        id: bookHeader
    }
        
    Component.onCompleted: openBook();

    Component.onDestruction: {
        //Save the state of the book before closing
        
        //Reset lastPosition for the next book to be open
        console.log("Reset lastPos")
        lastPosition = "null"
    }

    Component {
        id: bookCover

        BookCover {
            coverImage: bookPath + "/" + bookIndex.coverImage
        }
    }

    Component {
        id: bookText

        TextListView {
            //If lastPosition is not null, load the last section we were reading
            firstText: lastPosition === "null" ? bookIndex.bookStartsAt : lastPosition.split("|")[0]
        }
    }

    Loader {
        id: loadCover

        anchors { 
            fill: parent
            topMargin: bookHeader.height
        }
    }

    Loader {
        id: loadText

        //If true, when reseting story element loads behind the Header
        asynchronous: false

        anchors { 
            fill: parent
            //topMargin: bookHeader.height
            leftMargin: gridmargin
            rightMargin: gridmargin
            bottomMargin: gridmargin * 2
        }
    }

    Connections {
        target: loadCover.item

        onCoverClicked: {
            loadCover.sourceComponent = undefined
            loadText.sourceComponent = bookText
        }
    }

    function openBook() {
        bookHeader.title = bookIndex.bookTitle;

        //Check if it's not the first time we open this book
        lastPosition = DB.getPosition(bookIndex.bookTitle)

        if (lastPosition === "null") {
            lastPosition = bookIndex.bookStartsAt;
            DB.storePosition(new Date(), bookIndex.bookTitle, lastPosition);
        }

        loadCover.sourceComponent = bookCover;
    }
}
