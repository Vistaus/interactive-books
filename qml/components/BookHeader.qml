import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import QtQuick.LocalStorage 2.0

import "../js/db.js" as DB

CommonHeader {

    trailingActionBar {
		actions: Action {
            id: act
            shortcut: "ctrl+r"
            text: i18n.tr("Reset")
            onTriggered: {
                lastPosition = "null";
                DB.storePosition(new Date(), bookIndex.bookTitle, "null");
                loadText.sourceComponent = undefined;
                loadText.sourceComponent = bookText;
            }
        }
	}
    
    Connections {
        target: loadCover.item

        onCoverClicked: {
            act.iconName = "reset"
        }
    }
}
