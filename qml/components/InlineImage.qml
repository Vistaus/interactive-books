import QtQuick 2.9
import Ubuntu.Components 1.3

UbuntuShape {
    id: inlineImageShape
    width: parent.width
    height: image.height
    aspect: UbuntuShape.Flat
    sourceFillMode: UbuntuShape.PreserveAspectFit

    property url inlineImage

    source: Image {
        id: image
        sourceSize.width: mainPage.width > units.gu(45) ? units.gu(45) : units.gu(20)
        source: inlineImage
    }
}
