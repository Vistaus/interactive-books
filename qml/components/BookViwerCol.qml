import QtQuick 2.9
import Ubuntu.Components 1.3

Column {
    id: col
    width: parent.width
    anchors {
        top: parent.top
        topMargin: gridmargin
    }
    
    spacing: gridmargin
}
