import QtQuick 2.9
import Ubuntu.Components 1.3

UbuntuShape {
    id: bookCover
    property url coverImage
    signal coverClicked()

    anchors.fill: parent
    aspect: UbuntuShape.Flat

    source: Image {
        id: cover
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: coverImage
        
        onStatusChanged: { 
            if (cover.status == Image.Error) {
                cover.source = ".././../assets/floral.svg";
                tapText.text = i18n.tr("Tap to read")
            }
        }
    }

    sourceFillMode: UbuntuShape.PreserveAspectFit

    TextLabel {
        id: tapText
        horizontalAlignment: Text.AlignHCenter
        anchors {
            bottom: parent.bottom
            bottomMargin: gridmargin * 4
            horizontalCenter: parent.horizontalCenter
        }
    }

    MouseArea {
        anchors {
            fill: parent
            topMargin: bookHeader.height
        }
        
        onClicked: bookCover.coverClicked();
    }
}
