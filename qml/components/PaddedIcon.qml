import QtQuick 2.9
import Ubuntu.Components 1.3

UbuntuShape {
    property url coverImage

    aspect: UbuntuShape.Flat

    source: Image {
        id: storiesIcon
        anchors.horizontalCenter: parent.horizontalCenter
        sourceSize.width: units.gu(27)
        sourceSize.height: units.gu(10)
        asynchronous: true
        source: "../../assets/floral.svg"
    }

    sourceFillMode: UbuntuShape.Pad
}
