import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.LocalStorage 2.0

import "../js/db.js" as DB

ListItem {
    property string buttonText
    property string buttonLink

    divider.visible: false
    highlightColor: highLightColor
    width: parent.width + gridmargin * 2
    anchors.horizontalCenter: parent.horizontalCenter

    Label {
        id: optionLabel
        width: parent.width - (gridmargin*3) - nextIcon.width

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: gridmargin
        }

        text: buttonText
        wrapMode: Text.Wrap
        verticalAlignment: Text.AlignVCenter
    }

    Icon {
        id: nextIcon
        width: units.gu(2)

        anchors {
            verticalCenter: optionLabel.verticalCenter
            left: optionLabel.right
            leftMargin: gridmargin
        }

        height: width
        name: "next"
    }
    
    onClicked: {
        var newSection = bookPath + "/" + bookIndex.sections[buttonLink];
        newSection = newSection.split("|")[0];
        //ToDo: take care of section chapter > .split("|")[1]
        
        //Empty the column with the main content of the section
        col.children = "";
        secondCol.children = "";

        //Forces the header to pop but flicks the content properly
        mainFlick.contentY = -1 * bookHeader.height;

        //Save new position to db
        DB.storePosition(new Date(), bookIndex.bookTitle, bookIndex.sections[buttonLink]);

        importJson(newSection);
   }
}
