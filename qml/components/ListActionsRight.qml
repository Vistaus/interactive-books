import QtQuick 2.9
import Ubuntu.Components 1.3

ListItemActions {
    actions: [
        Action {
            iconName: "share"
            text: i18n.tr("Share")
            onTriggered: {
            }

        },
        Action {
            iconName: "save"
            text: i18n.tr("Save")
            onTriggered: {
            }
        }
    ]
}
