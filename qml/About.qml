/*
 * About template
 * By Joan CiberSheep using base file from uNav
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

import "components"

Page {
    id: aboutPage

    //Properties
    property string iconAppRute: "../assets/logo.svg"
    property string version: ""
    property string license: "<a href='https://www.gnu.org/licenses/gpl-2.0.html'>GPLv2</a>"
    property string source: "<a href='https://gitlab.com/cibersheep/interactive-books/'>Gitlab</a>"
    property string appName: "Interactive Books"

    header: CommonHeader {
        id: pageHeader
        title: i18n.tr("About")
        flickable: gameStoriesView
    }

    Flickable {
        id: flickable

        anchors {
            fill: parent
            bottomMargin: gridmargin
        }

        ListModel {
            id: gameStoriesModel

            Component.onCompleted: initialize()

            function initialize() {
                gameStoriesModel.append({ category: i18n.tr("App Development"), mainText: "Joan CiberSheep", secondaryText: "", link: "https://cibersheep.com/" })

                gameStoriesModel.append({ category: i18n.tr("Special Thanks to"), mainText: "Brian Douglass", secondaryText: i18n.tr("for his testing and ideas"), link: "http://bhdouglass.com/" })

                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("Main icon"), secondaryText: "CC-by Gregor Cresnar from the Noun Project", link: "https://thenounproject.com/" })
                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("Floral patterns"), secondaryText: "CC-By LSE Designs", link: "https://thenounproject.com/" })
                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("Floral patterns"), secondaryText: "CC-By Jacqueline Fernandes", link: "https://thenounproject.com/" })
                
                gameStoriesModel.append({ category: i18n.tr("Translations"), mainText: "Krakakanok", secondaryText: i18n.tr("Spanish Translation"), link: "https://gitlab.com/Krakakanok/" })
                gameStoriesModel.append({ category: i18n.tr("Translations"), mainText: "Anne Onyme 017", secondaryText: i18n.tr("French Translation and typos"), link: "https://gitlab.com/Anne17/" })
                gameStoriesModel.append({ category: i18n.tr("Translations"), mainText: "danfro", secondaryText: i18n.tr("German Translation"), link: "https://gitlab.com/Danfro/" })
                gameStoriesModel.append({ category: i18n.tr("Translations"), mainText: "Rocco Foti", secondaryText: i18n.tr("Italian Translation"), link: "https://gitlab.com/rocky58/" })
            }
        }

        ListView {
            id: gameStoriesView
            anchors.fill: parent
            header: listViewHeader
            model: gameStoriesModel
            delegate: listDelegate
            section.property: "category"
            section.criteria: ViewSection.FullString
            section.delegate: ListItemHeader {
                title: section
            }
        }
       
        Component {
            id: listViewHeader

            Item {
                width: parent.width
                height: iconTop.height + units.gu(24)

                UbuntuShape {
                    id: iconTop
                    width: units.gu(20)
                    height: width

                    anchors{
                        horizontalCenter: parent.horizontalCenter
                        top: parent.top
                        topMargin: units.gu(6)
                    }

                    aspect: UbuntuShape.Flat
                    source: Image {
                        source: iconAppRute
                    }
                }

                TextLabel {
                    id: appNameLabel
                    anchors.top: iconTop.bottom
                    anchors.topMargin: units.gu(4)
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr(appName)
                    font.bold: true
                }

                TextLabel {
                    id: appInfo
                    anchors.top: appNameLabel.bottom
                    anchors.topMargin: units.gu(2)
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Source %2").arg(source)
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: darkColor
                }

                TextLabel {
                    anchors.top: appInfo.bottom
                    anchors.topMargin: units.gu(2)
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("Under License %1").arg(license)
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: darkColor
                }
            }
        }
       
        Component {
            id: listDelegate
           
            ListItem {
                highlightColor: highLightColor
                divider.visible: false

                ListItemLayout {
                    id: storiesDelegateLayout
                    title.text: mainText
                    subtitle.text: secondaryText
                    ProgressionSlot { name: link !== "" ? "next" : ""}
                }

                onClicked: if (model.link !== "") Qt.openUrlExternally(model.link)
            }
        }
    }
}
