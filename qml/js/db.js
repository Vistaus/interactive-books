/*
 * uNav http://launchpad.net/unav
 * Copyright (C) 2015 JkB https://launchpad.net/~joergberroth
 * Copyright (C) 2015 Marcos Alvarez Costales https://launchpad.net/~costales
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
// Thanks http://askubuntu.com/questions/352157/how-to-use-a-sqlite-database-from-qml


function openDB() {
    var db = LocalStorage.openDatabaseSync("favcontacts_db", "0.1", "Read Books", 1000);

    try {
        db.transaction(function(tx){
            tx.executeSql('CREATE TABLE IF NOT EXISTS readBook( revision DATE, identifier INTEGER PRIMARY KEY, bookName TEXT, lastPosition TEXT )');
        });
    } catch (err) {
        console.log("Error creating table in database: " + err)
    } return db
}

// Save Current Position
function storePosition(revision, bookName, currentPosition) {
    var db = openDB();
    db.transaction(function(tx){
        tx.executeSql('INSERT OR REPLACE INTO readBook (revision, bookName, lastPosition) VALUES(?, ?, ?)', [revision, bookName, currentPosition]);
    });
}

function getPosition(book) {
    var lastPosition = "";
    var db = openDB();

    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT lastPosition FROM readBook WHERE bookName=? ORDER BY revision DESC;', [book]);

        if (rs.rows.length > 0) {
            lastPosition = rs.rows.item(0).lastPosition;
        } else {
            lastPosition = "null";
            console.log("getPosition: set to null");
        }
    });

    return lastPosition;
}

/*
function deleteContact(ident){
    var db = openDB();
    db.transaction( function(tx){
        tx.executeSql('DELETE FROM latestcontacts WHERE identifier=?;', [ident]);
    });
}
*/
