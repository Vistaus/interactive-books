function updateJSON(json) {
    for (var contentKey in json.content) {
        switch (contentKey) {
            case "main":
                qmlObjectToShow(json.content.main, col)
            break;

            case "secondary":
                qmlObjectToShow(json.content.secondary, secondCol)
            break;
        }
    }
}

function qmlObjectToShow(json, elementToRenderIn) {
    for (var i=0; i< json.length; ++i) {
        //DEBUG: console.log("Rendering the JSON: " + i + " of " + json.length + " - " + json[i])

        switch (JS.type(json[i])) {
            case "string":
                var component = Qt.createComponent("../components/TextLabel.qml");
                if (component.status == Component.Ready) {
                    var textLine = component.createObject(elementToRenderIn);
                    //This should be handled here to avoid error but it stops being adaptive
                    //textLine.width = parent.width
                    textLine.text = json[i];
                    //DEBUG: console.log("Type " + JS.type(json[i]))
                }
            break;
            
            case "array":
                console.log("array");
            break;
            
            case "object":
                //DEBUG: console.log("dictionary");
                for (var key in json[i]) {
                    //DEBUG: console.log(key);

                    switch (key) {
                        case "h2":
                            var component = Qt.createComponent("../components/TextLabel.qml");
                            if (component.status == Component.Ready) {
                                var textLine = component.createObject(elementToRenderIn);
                                textLine.font.bold = true;
                                textLine.text = json[i].h2;
                                //textLine.width = parent.width
                            }
                        break;
                        
                        case "centered":
                            var component = Qt.createComponent("../components/TextLabel.qml");
                            if (component.status == Component.Ready) {
                                var textLine = component.createObject(elementToRenderIn);
                                textLine.horizontalAlignment = Text.AlignHCenter;
                                textLine.text = json[i].centered;
                                //textLine.width = parent.width
                            }
                        break;

                        case "options":
                            //TODO: Don't forget to render lineBreak
                            
                            for (var j=0; j< json[i].options.length; ++j) {
                                //Handle visible property
                                if (json[i].options[j].hasOwnProperty("visible")) {
                                    //if the visible condition evaluation is false, skip the option
                                    if (eval(json[i].options[j].visible) == false) continue;
                                }
                                var component = Qt.createComponent("../components/OptionButton.qml");
                                if (component.status == Component.Ready) {
                                    var buttonOpt = component.createObject(elementToRenderIn);
                                    buttonOpt.buttonText = json[i].options[j].text;
                                    buttonOpt.buttonLink = json[i].options[j].target;
                                    //buttonOpt.width = parent.width
                                }
                            }
                        break;

                        case "image":
                            var component = Qt.createComponent("../components/InlineImage.qml");
                            if (component.status == Component.Ready) {
                                var imageElement = component.createObject(elementToRenderIn);
                                imageElement.inlineImage = bookPath + "/" + json[i].image;
                                //imageElement.width = parent.width
                            }
                        break;

                        case "list":
                           for (var k=0; k< json[i].list.length; ++k) {
                                //DEBUG: console.log("List " + json[i].list[k])
                                var component = Qt.createComponent("../components/UnorderedList.qml");
                                if (component.status == Component.Ready) {
                                    var list = component.createObject(elementToRenderIn);
                                    list.listText = json[i].list[k];
                                }
                            }
                        break;

                        case "followScene":
                            //TODO: Implement this 
                            for (var opt in json[i].followScene) {
                            }
                        break;
                    }
                }
            break;
        }
    }
}
